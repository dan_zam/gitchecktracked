#!/usr/bin/env python

import os
import sys
import subprocess

try:
    folder = sys.argv[1] +"/"
except:
    folder = ""

def decode_all(enc_list):
    if not isinstance(enc_list, list):
        return enc_list.decode('utf-8')
    else:
        return [e.decode('utf-8') for e in enc_list]

command = "git ls-files " + folder
process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
output, error = process.communicate()
tracked = decode_all(output.split(b'\n')[:-1])

command = "git ls-files --others " + folder
process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
output, error = process.communicate()
untracked = decode_all(output.split(b'\n')[:-1])

command = "ls -a " + folder
process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
output, error = process.communicate()
file_list = decode_all(output.split(b'\n')[:-1])


green = '\033[01;32m'
red = '\033[01;31m'
yellow = '\033[01;33m'
reset = '\033[00m'


tracked_marker = green + 'v' + reset
untracked_marker = red + 'x' + reset
unknown_marker = yellow + '?' + reset
folder_marker = yellow + '->' + reset

for el in file_list:
    el_foldered = folder + el
    if el_foldered in tracked:
        sys.stdout.write(tracked_marker+" "+el+"\n")
    elif el_foldered in untracked:
        sys.stdout.write(untracked_marker+" "+el+"\n")
    elif os.path.isdir(el_foldered):
        sys.stdout.write(folder_marker+" "+el+"\n")
    else:
        sys.stdout.write(unknown_marker+" "+el+"\n")

     